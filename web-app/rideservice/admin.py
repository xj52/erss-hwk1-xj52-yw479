from django.contrib import admin
from .models import Ride
from .models import Driver
from .models import Activity

# Register your models here.
admin.site.register(Ride)
admin.site.register(Driver)
admin.site.register(Activity)
