from django.forms import ModelForm, HiddenInput, DateTimeField, DateTimeInput
from django import forms
from .models import Ride, Driver
from django.core.validators import MinValueValidator

class RequestForm(ModelForm):
    arrivalTime = DateTimeField(input_formats=['%Y-%m-%dT%H:%M'],
                widget = DateTimeInput(
                attrs={'type': 'datetime-local'},
                format='%Y-%m-%dT%H:%M')) 

    class Meta:
        model = Ride
        widgets = {'ownerID': HiddenInput(), 'status': HiddenInput()}
        fields = ['destination', 'arrivalTime', 'numOfPassengers', 'vehicleType', 'message', 'isShare', 'ownerID', 'status']

class EditForm(ModelForm):
    arrivalTime = DateTimeField(input_formats=['%Y-%m-%dT%H:%M'],
                widget = DateTimeInput(
                attrs={'type': 'datetime-local'},
                format='%Y-%m-%dT%H:%M')) 
 
    class Meta:
        model = Ride
        fields = ['destination', 'arrivalTime', 'numOfPassengers', 'vehicleType', 'message', 'isShare', 'ownerID', 'status']
        labels = { # not work....
            "numOfPassengers": "changes of numOfPassengers",
        }
        widgets = {'ownerID': HiddenInput(), 'status': HiddenInput()}

    def __init__(self, *args, **kwargs):
        super(EditForm, self).__init__(*args, **kwargs)
        self.fields['numOfPassengers'].label = "changes of numOfPassengers"
        for name, field in self.fields.items():
            field.widget.attrs.update({'class': 'input'})

class SharerEditForm(ModelForm):
    class Meta:
        model = Ride
        fields = ['numOfPassengers']            

class DriverRegForm(forms.Form):
    vehicleType = forms.CharField(max_length=30, error_messages={'required': 'Vehicle Type cannot be empty.'})
    licensePlateNumber = forms.CharField(max_length=30, error_messages={'required': 'License Plate Number cannot be empty.'})
    maxPassenger = forms.IntegerField(initial=4, error_messages={'required': 'Max Passenger cannot be empty.'})
    specialInfo = forms.CharField(max_length=500)

class SharerSearchForm(forms.Form):
    destination = forms.CharField(max_length=200)
    earliestAcceptableArrivalTime = forms.DateTimeField(input_formats=['%Y-%m-%dT%H:%M'],
                widget = DateTimeInput(
                attrs={'type': 'datetime-local'},
                format='%Y-%m-%dT%H:%M')) 
    latestAcceptableArrivalTime = forms.DateTimeField(input_formats=['%Y-%m-%dT%H:%M'],
                widget = DateTimeInput(
                attrs={'type': 'datetime-local'},
                format='%Y-%m-%dT%H:%M'))             
    numOfPassengers = forms.IntegerField(validators=[MinValueValidator(1)])