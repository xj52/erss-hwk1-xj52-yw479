from django.urls import path
from django.contrib import admin
from . import views

urlpatterns = [
    path('', views.login, name='login'),
    path('signup/', views.signup, name='signup'),
    path('logout', views.logout, name='logout'),
    path('driverinfo/', views.driverinfo, name='driverinfo'),
    path('userinfo/', views.userinfo, name='userinfo'),
    path('useredit/<int:userID>', views.useredit, name='useredit'),
    path('driverregister/<int:userId>/', views.driverregister, name='driverregister'),
    path('driveredit/<int:userID>', views.driveredit, name='driveredit'),
    path('driverunregister', views.driverunregister, name='driverunregister'),
    path('main/', views.main, name='main'),
    path('request/', views.request, name='request'),
    path('<int:rideID>/driverStatus/', views.driverStatus, name='driverStatus'),
    path('<int:rideID>/passengerStatus/', views.passengerStatus, name='passengerStatus'),
    path('<int:rideID>/complete/', views.complete, name='complete'),
    path('<int:rideID>/passengerStatus/ownerEdit', views.ownerEdit, name='ownerEdit'),
    path('<int:rideID>/passengerStatus/sharerEdit', views.sharerEdit, name='sharerEdit'),
    path('<int:rideID>/passengerStatus/quit', views.quit, name='quit'),
    path('sharersearch/', views.sharerSearch, name='sharerSearch'),
    path('sharersearch/join/<int:rideID>/<int:num>', views.join, name='join'),
    path('driversearch/', views.driverSearch, name='driverSearch'),
    path('driversearch/confirm/<int:rideID>/', views.confirm, name='confirm'),
    path('sendemail', views.sendemail, name='sendemail'),
]