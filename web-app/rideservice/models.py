from django.db import models
# Django’s built-in authentication system 
from django.contrib.auth.models import User
from model_utils.fields import StatusField
from model_utils import Choices

# Create your models here.

class Driver(models.Model):
    driver = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    vehicleType = models.CharField(max_length=30)
    licensePlateNumber = models.CharField(max_length=30)
    maxPassenger = models.IntegerField(default=4)
    specialInfo = models.CharField(max_length=500, blank=True)
    def __str__(self):
        return self.driver.username + " with " + self.vehicleType

class Ride(models.Model):
    destination = models.CharField(max_length=200)
    arrivalTime = models.DateTimeField()
    numOfPassengers = models.IntegerField()
    vehicleType = models.CharField(max_length=30, blank=True)
    message = models.CharField(max_length=500, blank=True)
    isShare = models.BooleanField(default=True)
    driverID = models.ForeignKey(User, related_name='driverID', on_delete=models.CASCADE, null=True, blank=True) 
    ownerID = models.ForeignKey(User, related_name='ownerID', on_delete=models.CASCADE, blank=True) 
    sharerIDs = models.ManyToManyField(User, blank=True)
    STATUS = Choices('open', 'confirm', 'complete') 
    status = StatusField()
    def __str__(self):
        return self.destination + " from owner " + self.ownerID.username

class Activity(models.Model): 
    rideID = models.ForeignKey(Ride, related_name='rideID', on_delete=models.CASCADE)
     # ex: yw (owner) has a group of 2
     # ex: xj (sharer) has a group of 2
     # ex: xj (sharer) quit the ride
     # ex: xj (sharer) has a group of 1  
    message = models.CharField(max_length=500)
    time = models.DateTimeField()
    passengerChange = models.IntegerField(null=True, blank=True) # ex: 2 # ex: -1
    editor = models.ForeignKey(User, related_name='editor', on_delete=models.CASCADE, null=True, blank=True) # ownerID / sharerID 
    def __str__(self):
        return self.message
