# Generated by Django 4.1.5 on 2023-02-03 17:52

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('rideservice', '0008_alter_ride_ownerid'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ride',
            name='numOfPassengers',
            field=models.IntegerField(validators=[django.core.validators.MinValueValidator(1)]),
        ),
    ]
