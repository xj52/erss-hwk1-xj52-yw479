from datetime import datetime
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Driver, Ride, Activity
from django.contrib.auth.decorators import login_required
from .form import RequestForm, EditForm, SharerEditForm, DriverRegForm, SharerSearchForm
from django.contrib import messages
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.models import User
from django.db.models import Sum, Q
from django.template.defaulttags import register
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.conf import settings

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

# Create your views here.                 
def login(request):
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        
        if User.objects.filter(username=username).exists():
            user = authenticate(username=username, password=password)
            #user = authenticate(username=username, password=password)   # Django authenticate
            if user:
                auth_login(request, user)    #
                return HttpResponseRedirect('/main/')
            else:
                return render(request, 'login.html', {
                    'err_code': 'Invalid username/password'
                })
            """
            if password == user.password:  
                response = HttpResponseRedirect('/main/')
                request.session["info"]=user.user_id
                return response
            else:
                context = {
                    'errCode': 1
                } 
                return render(request,'login.html', context)
            """
        else:
            # messages.error(request, 'Please enter a valid username.')
            return render(request, 'login.html', {'err_code': 'Invalid username/password'})
       
    return render(request, 'login.html')

def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/')

def signup(request):
    if request.method =="POST":
        username = request.POST.get('username')
        if User.objects.filter(username=username).exists():
            return render(request, 'signup.html', {'err_code': 'Someone has already use this username.'})
        else:
            password = request.POST.get('password')
            email = request.POST.get('email')
            firstname = request.POST.get('firstname')
            lastname =request.POST.get('lastname')
            
            user = User.objects.create_user(username, email, password)
            user.first_name = firstname
            user.last_name = lastname
            """
            user = User()
            user.username = username
            user.password = password
            user.email = email
            """
            user.save()
            return HttpResponseRedirect('/')
    return render(request, 'signup.html')
    
@login_required
def userinfo(request):
    context = {
        'user': request.user
    }
    return render(request,'userinfo.html', context)

@login_required
def useredit(request, userID):
    if request.method =="POST":
        firstname = request.POST.get('firstname')
        lastname = request.POST.get('lastname')
        email = request.POST.get('email')
        
        
        user = get_object_or_404(User, id=userID)
        user.first_name = firstname
        user.last_name = lastname
        user.email = email
        user.save()
        return HttpResponseRedirect('/userinfo/')
    else:
        user = get_object_or_404(User, id=request.user.id)
        user = request.user
        context = {
            'user': user
        }
        return render(request,'useredit.html', context)

    return render(request, 'useredit.html')

@login_required
def driverinfo(request):
    if request.method =="POST":
        pass
    else:
        if Driver.objects.filter(driver = request.user).exists():
            driver = get_object_or_404(Driver, driver=request.user)
            user = request.user
            context = {
                'is_driver': 1,
                'driver':driver,
                'user': user
            }
            return render(request,'driverinfo.html', context)
        else:
            return render(request,'driverinfo.html')
    return render(request, 'driverinfo.html')

@login_required
def driverregister(request, userId):
    if request.method =="POST":
        vehicleType = request.POST.get('vehicleType')
        licensePlateNumber = request.POST.get('licensePlateNumber')
        maxPassenger = request.POST.get('maxPassenger')
        specialInfo = request.POST.get('specialInfo')
        
        driver = Driver()
        driver.driver = request.user
        driver.vehicleType = vehicleType
        driver.licensePlateNumber = licensePlateNumber
        driver.maxPassenger = maxPassenger
        driver.specialInfo = specialInfo
        driver.save()
        return HttpResponseRedirect('/driverinfo/')
    return render(request, 'driverregister.html')

@login_required
def driveredit(request, userID):
    if request.method =="POST":
        vehicleType = request.POST.get('vehicleType')
        licensePlateNumber = request.POST.get('licensePlateNumber')
        maxPassenger = request.POST.get('maxPassenger')
        specialInfo = request.POST.get('specialInfo')
        
        driver = get_object_or_404(Driver, driver=request.user)
        driver.driver = request.user
        driver.vehicleType = vehicleType
        driver.licensePlateNumber = licensePlateNumber
        driver.maxPassenger = maxPassenger
        driver.specialInfo = specialInfo
        driver.save()
        return HttpResponseRedirect('/driverinfo/')
    else:
        driver = get_object_or_404(Driver, driver=request.user)
        user = request.user
        context = {
            'driver':driver,
            'user': user
        }
        return render(request,'driveredit.html', context)

    return render(request, 'driverregister.html')

@login_required
def driverunregister(request):
    if request.method =="POST":
        #userId = request.POST['userId']
        driver = Driver.objects.get(driver_id=request.user.id)
        driver.delete()
        #messages.success(request, 'You have success!')
        return HttpResponseRedirect('/driverinfo/')

    
@login_required
def sendemail(request):
    if request.method =="POST":
        user = get_object_or_404(User, id=request.user.id)
        driver = get_object_or_404(User, username= 'test_reg_2')
        to_email = driver.email
        from_email = user.email
        send_mail('Ride Confirmation', 'The driver has confirmed your trip', from_email, [to_email],fail_silently=False)
        return HttpResponseRedirect('/main/')

# show user's ride list (main page) 
@login_required
def main(request):
    ownerList = list(Ride.objects.order_by('arrivalTime').filter(ownerID=request.user).exclude(status='complete'))
    sharerList = list(Ride.objects.order_by('arrivalTime').filter(sharerIDs__id=request.user.id).exclude(status='complete'))
    driverList = list(Ride.objects.order_by('arrivalTime').filter(driverID=request.user).exclude(status='complete') )
    user = request.user
    context = {
        'ownerList': ownerList, 
        'sharerList': sharerList,
        'driverList' : driverList,
        'user':user,
    } 
    return render(request, 'main.html', context)

# request ride
@login_required
def request(request):
    if request.method == 'POST':
        form = RequestForm(request.POST)
        if form.is_valid():
            requestTime = datetime.strptime(request.POST['arrivalTime'], '%Y-%m-%dT%H:%M')
            if requestTime < datetime.now():
                err_msg = 'You cannot choose a time earlier than the present.'
                #messages.warning(request, 'You cannot choose a time earlier than the present.')
                return render(request, 'request.html', {'form': form, 'err_msg': err_msg})
            sameRequestTime = Ride.objects.filter(ownerID=request.user, arrivalTime=request.POST['arrivalTime']).exists()
            if sameRequestTime:
                err_msg = 'You have already requested a ride with this arrival time.'
                #messages.warning(request, 'You have already requested a ride with this arrival time.')
                return render(request, 'request.html', {'form': form, 'err_msg': err_msg})      
            else:    
                form = form.save(commit=False)
                form.ownerID = request.user 
                form.status = 'open' 
                form.save()
                activity = Activity()
                activity.rideID = get_object_or_404(Ride, ownerID=request.user, arrivalTime=form.arrivalTime)
                activity.message = request.user.get_username() + ' (owner) joined as a group of ' + str(form.numOfPassengers)
                activity.time = datetime.now()
                activity.passengerChange = form.numOfPassengers
                activity.editor = request.user
                activity.save()  
                # redirect to a new URL:
                return HttpResponseRedirect('/main/')    
    # if a GET (or any other method) we'll create a blank form
    else:
        form = RequestForm()
    return render(request, 'request.html', {'form': form})

@login_required
def driverStatus(request, rideID):
    ride = get_object_or_404(Ride, pk=rideID)
    User = get_user_model()
    # how many passengers in each group
    editors = list(Activity.objects.filter(rideID=rideID).order_by().values_list('editor', flat=True).distinct())
    groups = {}
    for editor in editors:
        num = Activity.objects.filter(rideID=rideID, editor=editor).aggregate(Sum('passengerChange'))['passengerChange__sum']
        user = get_object_or_404(User, pk=editor)
        groups[user.get_username()] = num # key is username 
    context = {
        'ride' : ride,
        'groups' : groups,
    } 
    return render(request, 'driverstatus.html', context)

@login_required
def passengerStatus(request, rideID):
    ride = get_object_or_404(Ride, pk=rideID)
    if ride.status == 'confirm':
        vehicle = get_object_or_404(Driver, pk=ride.driverID)
    else:
        vehicle = ''   
    messages = list(Activity.objects.order_by('-time').filter(rideID=rideID)) 
    context = {
        'ride' : ride,
        'vehicle' : vehicle,
        'messages' : messages,
        'requestID' : request.user, 
    }  
    return render(request, 'passengerstatus.html', context)

@login_required
def complete(request, rideID):
    ride = get_object_or_404(Ride, pk=rideID)
    ride.status = 'complete'
    ride.save()
    return HttpResponseRedirect('/main/')    

@login_required
def ownerEdit(request, rideID):
    if request.method == 'POST':
        ride = get_object_or_404(Ride, pk=rideID)
        ridePassNum = ride.numOfPassengers 
        form = EditForm(request.POST, instance=ride) 
        if form.is_valid():
            requestTime = datetime.strptime(request.POST['arrivalTime'], '%Y-%m-%dT%H:%M')
            if requestTime < datetime.now():
                err_msg = 'You cannot choose a time earlier than the present.'
                #messages.warning(request, 'You cannot choose a time earlier than the present.')
                return render(request, 'ownerEdit.html', {'form': form, 'err_msg': err_msg})
            sameRequestTime = Ride.objects.filter(ownerID=request.user, arrivalTime=request.POST['arrivalTime']).exclude(pk=rideID).exists()
            if sameRequestTime:
                err_msg ='You have already requested a ride with this arrival time.'
                #messages.warning(request, 'You have already requested a ride with this arrival time.')
                return render(request, 'ownerEdit.html', {'form': form, 'err_msg': err_msg})      
            else:   
                num = form.cleaned_data['numOfPassengers'] + ridePassNum
                form = form.save(commit=False)
                form.ownerID = request.user 
                form.status = 'open'
                if form.numOfPassengers != 0:
                    activity = Activity()
                    activity.rideID = ride
                    activity.message = request.user.get_username() + ' (owner) acted ' + str(form.numOfPassengers) + ' to the number of his/her group'
                    activity.time = datetime.now()
                    activity.passengerChange = form.numOfPassengers # numOfPassengers is the variety of owner's group
                    activity.editor = request.user
                    activity.save()  
                    form.numOfPassengers = num
                if ride.isShare == True & form.isShare == False & (not ride.sharerIDs is None):
                    for sharer in ride.sharerIDs.all():
                        Activity.objects.filter(rideID=rideID, editor=sharer).delete()    
                    ownerPassNum = Activity.objects.filter(rideID=rideID, editor=request.user).aggregate(Sum('passengerChange'))['passengerChange__sum'] 
                    ride.sharerIDs.clear()
                    activity = Activity()
                    activity.rideID = ride
                    activity.message = request.user.get_username() + ' (owner) changed this ride to not shared'
                    activity.time = datetime.now()
                    activity.passengerChange = 0
                    activity.editor = request.user
                    activity.save() 
                    ride.save()
                    form.numOfPassengers = ownerPassNum 
                form.save()

                # send email to notify sharer
                sharelist = ride.sharerIDs.all()
                email_list = []
                for s in sharelist:
                    email_list.append(s.email) 
                send_mail('Ride Notification', 'The owner of the trip has edited it. Please go to the website to see if you need to change your plan.', settings.EMAIL_HOST_USER, email_list, fail_silently=False)
                return HttpResponseRedirect('/main/')    
    # if a GET (or any other method) we'll create a blank form
    else:
        form = EditForm()
    return render(request, 'ownerEdit.html', {'form': form})

@login_required
def sharerEdit(request, rideID):
    if request.method == 'POST':
        ride = get_object_or_404(Ride, pk=rideID)
        ridePassNum = ride.numOfPassengers 
        form = SharerEditForm(request.POST, instance=ride) 
        if form.is_valid():
            sharerPassNum = Activity.objects.filter(rideID=rideID, editor=request.user).aggregate(Sum('passengerChange'))['passengerChange__sum'] 
            if (form.cleaned_data['numOfPassengers'] + sharerPassNum) <= 0:
                err_msg = 'Your group passenger number has to be a positive number.'
                #messages.warning(request, 'Your group passenger number has to be a positive number.')
                return render(request, 'sharerEdit.html', {'form': form, 'err_msg': err_msg}) 
            form = form.save(commit=False)
            activity = Activity()
            activity.rideID = ride
            activity.message = request.user.get_username() + ' (sharer) acted ' + str(form.numOfPassengers) + ' to the number of his/her group'
            activity.time = datetime.now()
            activity.passengerChange = form.numOfPassengers 
            activity.editor = request.user
            activity.save()  
            form.numOfPassengers = form.numOfPassengers + ridePassNum 
            form.save()
            # redirect to a new URL:
            return HttpResponseRedirect('/main/')    
    # if a GET (or any other method) we'll create a blank form
    else:
        form = SharerEditForm()
    return render(request, 'sharerEdit.html', {'form': form})

@login_required
def quit(request, rideID):
    ride = get_object_or_404(Ride, pk=rideID)
    if request.user == ride.ownerID: # owner
        email_list = []
        sharelist = ride.sharerIDs.all()
        for s in sharelist:
            email_list.append(s.email) 
        send_mail('Ride Notification', 'The owner of the trip has cancelled it. Please go to the website to see if you need to change your plan.', settings.EMAIL_HOST_USER, email_list, fail_silently=False)
        ride.delete()
        Activity.objects.filter(rideID=rideID).delete()
    else: # sharer
        # delete from manytomanyfield
        sharerPassNum = Activity.objects.filter(rideID=rideID, editor=request.user).aggregate(Sum('passengerChange'))['passengerChange__sum']  
        Activity.objects.filter(rideID=rideID, editor=request.user).delete()
        ride.numOfPassengers -= sharerPassNum
        ride.sharerIDs.remove(request.user)
        ride.save()
    return HttpResponseRedirect('/main/')

@login_required
def sharerSearch(request):
    if request.method == 'POST': 
        form = SharerSearchForm(request.POST) 
        if form.is_valid():
            lo = form.cleaned_data['earliestAcceptableArrivalTime']
            hi = form.cleaned_data['latestAcceptableArrivalTime']
            rideList = list(Ride.objects.order_by('arrivalTime')
                .filter(status='open', arrivalTime__range=(lo, hi), destination=form.cleaned_data['destination'], isShare=True)
                .exclude(Q(ownerID=request.user) | Q(driverID=request.user) | Q(sharerIDs=request.user))) 
            context = {
                'rideList' : rideList,
                'num' : form.cleaned_data['numOfPassengers'],
            }  
            # redirect to a new URL:
            return render(request, 'sharersearchres.html', context)
    # if a GET (or any other method) we'll create a blank form
    else:
        form = SharerSearchForm()
    return render(request, 'sharersearch.html', {'form': form}) 

@login_required
def join(request, rideID, num):
    ride = get_object_or_404(Ride, pk=rideID) 
    activity = Activity()
    activity.rideID = ride
    activity.message = request.user.get_username() + ' (sharer) joined as a group of ' + str(num)
    activity.time = datetime.now()
    activity.passengerChange = num
    activity.editor = request.user
    activity.save()  
    # Ride update
    ride.numOfPassengers += num
    ride.sharerIDs.add(request.user)
    ride.save()
    return HttpResponseRedirect('/main/')

@login_required
def driverSearch(request):
    if Driver.objects.filter(driver=request.user).exists(): 
        vehicle = get_object_or_404(Driver, pk=request.user)
        rideList = list(Ride.objects.order_by('arrivalTime')
            .filter(status='open', numOfPassengers__lte=vehicle.maxPassenger)
            .exclude(Q(ownerID=request.user) | Q(sharerIDs=request.user))) 
        # vehicle type / special info, if applicable
        rideList = [ride for ride in rideList if (ride.vehicleType == "") or (ride.vehicleType != "" and ride.vehicleType == vehicle.vehicleType)] 
        rideList = [ride for ride in rideList if (ride.message == "") or (ride.message != "" and ride.message == vehicle.specialInfo)] 
        return render(request, 'driversearch.html', {'rideList' : rideList})
    else:
        #messages.warning(request, "You need to register as a driver to take a ride")
        return HttpResponseRedirect('/driverinfo/')     

@login_required
def confirm(request, rideID):
    ride = get_object_or_404(Ride, pk=rideID) 
    ride.status = 'confirm'
    ride.driverID = request.user
    ride.save()
    # send email
    ownerUsername = ride.ownerID
    owner = get_object_or_404(User, username=ownerUsername) 
    owner_email = owner.email
    sharelist = ride.sharerIDs.all()
    email_list = [owner_email]
    for s in sharelist:
        email_list.append(s.email) 
    send_mail('Ride Confirmation', 'Your driver has confirmed your trip!', settings.EMAIL_HOST_USER, email_list, fail_silently=False)
    return HttpResponseRedirect('/main/')