Solved:
- If we don't make sure that the user has already logged in, our website could be hacked or messed up. Thus, we 
used login_required decorator to guarantee that the user has logged in when he or she take some actions, which 
improves system security.

- Users may create duplicate usernames. We implemented special checking procedures during the registration phase 
to prevent duplicate usernames. 

- It is possible for multiple drivers to confirm the same ride at the same time. And multiple sharer may join the 
same ride at the same time. After consulting with TA, she told us that Django is single-threaded and will order 
simultaneous instructions so that the program doesn't crash.

- When the user enters “num of passengers”, we use a validator to ensure that the input must be positive to avoid 
unreasonable ride information. 

- We found that a ride could have multiple sharers, so we changed our sharer field from CharField to ManyToManyField.

- At first, we wanted to use labels in ModelForm to give our user some hints and make our website more user-friendly. 
However, the labels didn't work well although we tried several solutions. In the ended, we manually edited the 
corresponding template file to fix this problem. 

- When we transfer from sharer search result page to sharer join page, we noticed that we couldn't transfer the number
of passengers the user input before. We figured out this by add this variable to the new url, so that we can get this 
variable in the sharer join page.



Improve robustness in the future: 
-  When a driver confirms a ride, it's possible that he or she already had a ride with the same arrival
time as an owner or a sharer. So I need to add more logic to avoid this circumstance.

- Change the type of vehicle type from CharField to Choices, so that we can link the vehicle capacity with 
vehicle type. In this way, we can avoid the situation where the user fills in vehivle capacity randomly.

- Some user's inputs are not strictly checked, such as whether the license plate number is valid, or whether 
the destination they put in when requesting a ride is valid.

- When the owner changes destination, we can automatically make the sharer exit the ride and notify the sharer 
by email, instead of sending an email to the sharer to tell them to manually exit the ride. 

- When the owner changes the number of people in his or her group, we need some smarter algorithms to decide 
which sharer to let out, to maximize sharers’ equity.

- If the user don't provide all the fields in the sign up page, our website would crash. We can add more logic to
avoid this situation.

Action that would result error:
-All Form field is required (except 'special info', 'message'), if there is no input, an 404 error will occur

